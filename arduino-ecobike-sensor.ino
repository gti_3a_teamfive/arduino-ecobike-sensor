/***************************************************************************************************
****************************************************************************************************
  arduino-ecobike-sensor.ino
  Proyecto medio ambiente

  Aplicación arduino
    Consulta el sensor de gas por puerto serie mediante la librería SensorCO.h
    Transmite las medidas por Beacons BLE mediante la librería EmisoraBLE.h

  Equipo 5

****************************************************************************************************
***************************************************************************************************/

/* LIBRERIAS Y DEFINICIONES
/**************************************************************************************************/
#include "SensorCO.h"
#include "EmisoraBLE.h"
/**************************************************************************************************/

/* Declaración de VARIABLES de clase
/**************************************************************************************************/
SensorCO miSensor(&Serial1);
EmisoraBLE miEmisoraBLE;
/**************************************************************************************************/

/* SETUP
/**************************************************************************************************/
void setup()
{
  // Inicializar todo
  Serial.begin(115200);
  /*
  while (!Serial)
  {
    delay(10);  // wait for serial port to connect. Needed for Native USB only
  }
  */  
  Serial1.begin(9600);  // inicializa el puerto serie donde se conecta el sensor
  Serial.flush();
  Serial1.flush();
  Serial.println("Iniciando Setup");
  miEmisoraBLE.inicializar();   // Aquí inicio el BT LE y empieza a emitir
  Serial.print("Esperando 10 segundos");
  for (size_t i = 0; i < 10; i++)
  {
    delay(1000);
    Serial.print(".");
  }
  Serial.println();
  miSensor.inicializar();       // Pide el numero de serie del sensor
  miEmisoraBLE.anunciarNumeroSerie(miSensor.dimeNumeroSerie());    // lo transmite por BT
  Serial.println("Listo");
}
/**************************************************************************************************/

/* LOOP
/**************************************************************************************************/
void loop()
{
  // Mide y actualiza el emisor BT LE con las nuevas medidas cada 5 segundos
  Serial.println("Lectura del sensor iniciada...");
  medirYPublicar();
  delay(5000);
}
/**************************************************************************************************/

/* MEDIR Y PUBLICAR
/**************************************************************************************************/
void medirYPublicar()
{
  uint32_t ppb = miSensor.medirCO();  // Concentración de CO en partes por billón americano
  int8_t temp = miSensor.dimeTemp();  // Temperatura en grados centígrados
  uint8_t rh = miSensor.dimeRH();     // Humedad relativa en %

  // Muestro las medidas por puerto serie para debug
  Serial.println("========================================");
  Serial.print("  Concentración de CO: \t");
  Serial.print(ppb);
  Serial.println(" ppb");
  Serial.print("  Temperatura: \t\t");
  Serial.print(temp);
  Serial.println(" ºC");
  Serial.print("  Humedad relativa: \t");
  Serial.print(rh);
  Serial.println(" %");
  Serial.println("========================================");

  miEmisoraBLE.anunciarCOyTemp(ppb, temp);  // Actualizar el beacon con las medidas
}
/**************************************************************************************************/
