/*
  SensorCO.h - Librería para el sensor de gas SPEC
  Recibe medidas del sensor mediante puerto serie
  Creado por Joan Lluís Fuster, Septiembre 2019.
*/

#ifndef SensorCO_h
#define SensorCO_h

#include "Arduino.h"
#include "Stream.h"

class SensorCO
{
private:
  Stream *_mySerial;             // puntero al puerto serie donde está conectado el sensor
  String serialNumber;           // numero de serie del sensor
  uint32_t ppb;                  // almacena la concentración de gas en ppb
  int8_t temp;                   // almacena la temperatura
  uint16_t dataArray[8];         // Array para almacenar el resto de los datos recibidos
  String dataString = "";        // String recibido con los datos separados por comas
  boolean dataReceived = false;  // Flag para el control del parseo y guardado

  void cacharroDimeloTodo();

  void read_data();
  void parse_data();
  void print_data();

public:
  SensorCO(Stream *mySerial);
  void inicializar();

  String dimeNumeroSerie();
  uint32_t medirCO();
  int8_t dimeTemp();
  uint8_t dimeRH();
  void dimeHora();
};

#endif
