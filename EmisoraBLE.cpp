/*
  EmisoraBLE.h - Librería para crear beacons BLE con las medidas de gas
  Creado por Joan Lluís Fuster, Septiembre 2019.
*/

#include "EmisoraBLE.h"

/* Constructor (a su vez crea un beacon con major=minor=0 necesario en el setup())
/**************************************************************************************************/
EmisoraBLE::EmisoraBLE() : myBeacon(beaconUuid, _major, _minor, _rssi)
{
}
/**************************************************************************************************/

/* Emite el beacon por Bluetooth Low Energy
/**************************************************************************************************/
void EmisoraBLE::startAdv()
{
  Bluefruit.setName("GTI-3A-Equipo_5");
  // Advertising packet
  // Set the beacon payload using the BLEBeacon class populated earlier
  Bluefruit.Advertising.setBeacon(myBeacon);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * Apple Beacon specs
   * - Type: Non connectable, undirected
   * - Fixed interval: 100 ms -> fast = slow = 100 ms
  */
  // Bluefruit.Advertising.setType(BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED);
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(160, 160); // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);    // number of seconds in fast mode
  Bluefruit.Advertising.start(0);              // 0 = Don't stop advertising after n seconds
}
/**************************************************************************************************/

/* Emite el numero de serie en el UUID del beacon
/**************************************************************************************************/
void EmisoraBLE::anunciarNumeroSerie(String numeroSerie)
{
  char datos[14];
  numeroSerie.toCharArray(datos, 14);
  // ToDo: insertar numero de serie en UUID

  // AirLocate UUID: E2C56DB5-DFFB-48D2-B060-122118010902
  // 122118010902 es el numero de serie del sensor
  const uint8_t newUuid[16] =
  {
    0xE2, 0xC5, 0x6D, 0xB5, 0xDF, 0xFB, 0x48, 0xD2,
    0xB0, 0x60, 0x12, 0x21, 0x18, 0x01, 0x09, 0x02
  };
  //myBeacon.setUuid(newUuid);                  // Actualiza el UUID en el beacon BLE
  //myBeacon.start();
  //Bluefruit.Advertising.setBeacon(myBeacon);  // Emite el nuevo beacon actualizado
}
/**************************************************************************************************/

/* Inserta la concentración de CO y la temperatura en el Beacon
/**************************************************************************************************/
void EmisoraBLE::anunciarCOyTemp(uint32_t ppb, int8_t temp)
{
  // Necesarios 20 bits para ppb, 8 bits para temp
  // Mando la temperatura en el primer byte del major
  // Mando la concentración usando 3 bytes: el segundo del major + dos del minor
  // VAN AL REVÉS POR QUE POR LO VISTO LA CLASE BEACON LOS TRATA COMO LITTLE ENDIAN

  // temperatura al segundo byte del major, primer byte del ppb para el primer byte del major
  signed char majorBytes[2];
  majorBytes[1] = temp;
  majorBytes[0] = ((ppb >> 16) & 0xff);
  uint16_t major = (majorBytes[0] << 8) | (majorBytes[1]);

  // segundo y tercer byte del ppb invertidos al minor
  signed char minorBytes[2];
  minorBytes[0] = ppb & 0xff;
  minorBytes[1] = ((ppb >> 8) & 0xff);
  uint16_t minor = (minorBytes[0] << 8) | (minorBytes[1]);

  myBeacon.setMajorMinor(major, minor);       // Actualiza los valores en el beacon BLE
  Bluefruit.Advertising.setBeacon(myBeacon);  // Emite el nuevo beacon actualizado
}
/**************************************************************************************************/

/* Inicializa el módulo BT (Hay que invocarla en el SETUP())
/**************************************************************************************************/
void EmisoraBLE::inicializar()
{
  Serial.println("Initialising Bluefruit nRF52 module...");
  Serial.println("UUID: ");
  for (int i = 0; i < 16; i++){
    Serial.print( beaconUuid[i] );
  }
  Serial.println();
  Bluefruit.begin();
  // off Blue LED for lowest power consumption
  Bluefruit.autoConnLed(false);
  // Set max power. Accepted values are: -40, -30, -20, -16, -12, -8, -4, 0, 4
  Bluefruit.setTxPower(0);
  startAdv();
}
/**************************************************************************************************/
