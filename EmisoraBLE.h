/*
  EmisoraBLE.h - Librería para crear beacons BLE con las medidas de gas
  Creado por Joan Lluís Fuster, Septiembre 2019.
*/

#ifndef EmisoraBLE_h
#define EmisoraBLE_h

#include "Arduino.h"
#include <bluefruit.h>

// 0x004C is Apple (for example)
#define MANUFACTURER_ID 0x004C
// #define MANUFACTURER_ID 0xFFFF  // reserved for test use

// AirLocate UUID: E2C56DB5-DFFB-48D2-B060-000000000000
const uint8_t beaconUuid[16] =
{
  0xE2, 0xC5, 0x6D, 0xB5, 0xDF, 0xFB, 0x48, 0xD2,
  0xB0, 0x60, 0x12, 0x21, 0x18, 0x01, 0x09, 0x02
};

class EmisoraBLE
{
private:
  uint16_t _major = 0x0000;
  uint16_t _minor = 0x0000;
  int8_t _rssi = -54;
  BLEBeacon myBeacon;

  void startAdv();

public:
  EmisoraBLE();
  void inicializar();

  void anunciarNumeroSerie(String numeroSerie);
  void anunciarCOyTemp(uint32_t ppb, int8_t temp);
  
};

#endif
