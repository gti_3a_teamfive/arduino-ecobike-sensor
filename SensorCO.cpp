/*
  SensorCO.h - Librería para el sensor de gas SPEC
  Recibe medidas del sensor mediante puerto serie
  Creado por Joan Lluís Fuster, Septiembre 2019.
*/

#include "SensorCO.h"

/* Constructor (recibe un puntero al puerto serie)
/**************************************************************************************************/
SensorCO::SensorCO(Stream *mySerial) : _mySerial(mySerial)
{
}
/**************************************************************************************************/

/* Pide una medida para recibir el numero de serie del sensor
/**************************************************************************************************/
void SensorCO::inicializar()
{
  Serial.println("Initialising Sensor module...");
  cacharroDimeloTodo();
  delay(500);
  Serial.print("SerialNumber: ");
  Serial.println(serialNumber);
}
/**************************************************************************************************/

/* Realiza una lectura mediante read_data() y la guarda en las variables privadas con parse_data()
/**************************************************************************************************/
void SensorCO::cacharroDimeloTodo()
{
  while (_mySerial->available())
    _mySerial->read();
  read_data();
  if (dataReceived)
  {
    parse_data();
    // print_data();       // para debug
    dataReceived = false;  // Una vez parseada y guardada la medida pongo el flag a 0
  }
}
/**************************************************************************************************/

/* Recibe una lectura del sensor por puerto serie
/**************************************************************************************************/
void SensorCO::read_data()
{
  dataReceived = false;    // Flag puesto a "false" antes de solicitar una medida
  dataString = "";         // Limpiamos el String que almacena los caracteres recibidos
  _mySerial->print('\r');  // Mandamos retorno de carro para solicitar una medida
  delay(100);              // Esperamos a que responda el sensor
  dataString = _mySerial->readStringUntil('\n');  // Leemos hasta caracter "new line"
  if (dataString.length() > 0)
  {
    dataReceived = true;   // Si hay algo pongo el Flag a 1
  }
}
/**************************************************************************************************/

/* Parsea la cadena de datos y los guarda
/**************************************************************************************************/
void SensorCO::parse_data()
{
  // Parses the received dataString. DATA STRING IS COMMA SEPARATED
  // The format of the output is: SN[XXXXXXXXXXXX], PPB[0 : 999999], TEMP[-99:99], RH[0:99],
  // RawSensor[ADCCount], TempDigital, RHDigital, Day[0:99], Hour[0:23], Minute[0:59], Second[0:59]\r\n

  // Número de serie del sensor
  int index1 = dataString.indexOf(',');
  serialNumber = dataString.substring(0, index1);

  // Concentración de gas en ppb
  int index2 = dataString.indexOf(',', index1 + 1);
  String S_gas = dataString.substring(index1 + 2, index2);  // Hint: after comma there's a space - it should be ignored
  ppb = S_gas.toInt();

  // Temperatura en grados centígrados
  int index3 = dataString.indexOf(',', index2 + 1);
  String S_temp = dataString.substring(index2 + 2, index3);
  temp = S_temp.toInt();

  // Resto de medidas
  int indexLast = index3;
  for (int i = 0; i < 7; i++)
  {
    int index = dataString.indexOf(',', indexLast + 1);
    String miString = dataString.substring(indexLast + 2, index);
    dataArray[i] = miString.toInt();
    indexLast = index;
  }
  int indexEnd = dataString.indexOf('\r');
  String S_Seconds = dataString.substring(indexLast + 2, indexEnd);
  dataArray[7] = S_Seconds.toInt();
}
/**************************************************************************************************/

/* Muestra los datos en pantalla para DEBUG
/**************************************************************************************************/
void SensorCO::print_data()
{
  Serial.println("********************************************************************");
  Serial.print("Sensor Serial No. is ");
  Serial.println(serialNumber);
  Serial.print("CO level is ");
  Serial.print(ppb);
  Serial.println(" ppb");
  Serial.print("Temperature is ");
  Serial.print(temp, DEC);
  Serial.println(" deg C");
  Serial.print("Humidity is ");
  Serial.print(dataArray[0], DEC);
  Serial.println("% RH");
  Serial.print("Sensor is online since: ");
  Serial.print(dataArray[4], DEC);
  Serial.print(" days, ");
  Serial.print(dataArray[5], DEC);
  Serial.print(" hours, ");
  Serial.print(dataArray[6], DEC);
  Serial.print(" minutes, ");
  Serial.print(dataArray[7], DEC);
  Serial.println(" seconds");
  Serial.println("Raw Sensor Data");
  Serial.print("Raw gas level: ");
  Serial.println(dataArray[1]);
  Serial.print("Temperature digital: ");
  Serial.println(dataArray[2]);
  Serial.print("Humidity digital: ");
  Serial.println(dataArray[3]);
  Serial.println("********************************************************************");
}
/**************************************************************************************************/

/* Devuelve el numero de serie del sensor
/**************************************************************************************************/
String SensorCO::dimeNumeroSerie()
{
  return serialNumber;
}
/**************************************************************************************************/

/* Devuelve la concentración de CO
/**************************************************************************************************/
uint32_t SensorCO::medirCO()
{
  cacharroDimeloTodo();
  return ppb;
}
/**************************************************************************************************/

/* Devuelve la temperatura
/**************************************************************************************************/
int8_t SensorCO::dimeTemp()
{
  return temp;
}
/**************************************************************************************************/

/* Devuelve la humedad relativa
/**************************************************************************************************/
uint8_t SensorCO::dimeRH()
{
  return dataArray[0];
}
/**************************************************************************************************/

/* Devuelve la hora de la medida
/**************************************************************************************************/
void SensorCO::dimeHora()
{
  // para futuros usos podría retornar la hora de la medida en millis
}
/**************************************************************************************************/
